CC = gcc
CFLAGS= -c -I.
DEPS = point.h
CFLAG1 = -lm
all: final

final: main.o point.o
	$(CC) -o final main.o point.o -I. -lm

%.o: %.c $(DEPS)
	$(CC) $(CFLAGS) -o $@ $< $(CFLAG1)

clean:
	rm *o final
